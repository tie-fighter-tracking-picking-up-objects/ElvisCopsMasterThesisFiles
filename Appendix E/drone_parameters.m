%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% License
% 
% Copyright (c) 2020 David Wuthier (dwuthier@gmail.com)
% 
% "Proprietary software"
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Simulation of a quadcopter inspired from the paper
%
% Mellinger, Daniel, and Vijay Kumar.
% "Minimum snap trajectory generation and control for quadrotors."
% 2011 IEEE International Conference on Robotics and Automation. IEEE, 2011.
%
% To use this program: run the simulink model (drone_simulation.slx)

%% INITIALIZATION

clear
close all
clc

load('drone_parameters.mat')

%% SIMULATION PARAMETERS

sample_time = 0.04;
publish_rate = 1 * sample_time;
g = 9.80665 ;
mass_drone = 4.65;
mass_rod = 0.0;
mass_tip = 0;
mass_total = mass_drone;
inertia_xx = 0.007 ;
inertia_yy = 0.007 ;
inertia_zz = 0.012 ;
arm_length = 0.37 ;
rotor_offset_top = 0.01 ;
motor_constant = 8.54858e-06 ;
moment_constant = 0.016 ;
max_rot_velocity = 7910;
allocation_matrix = ...
    [1 1 1 1
     0 arm_length 0 -arm_length
     -arm_length 0 arm_length 0
     -moment_constant moment_constant -moment_constant moment_constant] ;
mix_matrix = inv(motor_constant * allocation_matrix) ;
xb0 = -3;
yb0 = 0;

%% Create Simulink model workspace

save('drone_parameters.mat')
